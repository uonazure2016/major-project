# -*- mode: ruby -*-
# vi: set ft=ruby :

# Determine operating system
module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
end

Vagrant.configure("2") do |config|
    # We use Ubuntu 14.04 due to it's stability and mature nature
    config.vm.box = "ubuntu/trusty64"

    # Name for Vagrant to use instead of 'default'
    config.vm.define :major_project

    # Forwarded port mapping for Vagrant box
    config.vm.network "forwarded_port", guest: 8080, host: 29873

    # Extra VirtualBox settings
    config.vm.provider "virtualbox" do |vb|

        vb.name = "Major Project"

        vb.customize ["modifyvm", :id, "--ioapic", "on"]
        host = RbConfig::CONFIG['host_os']

        # Give VM 1/4 system memory & access to all CPU cores on the host
        if host =~ /darwin/
            cpus = `sysctl -n hw.ncpu`.to_i
            mem = `sysctl -n hw.memsize`.to_i / 1024 / 1024 / 4
        elsif host =~ /linux/
            cpus = `nproc`.to_i
            mem = `grep 'MemTotal' /proc/meminfo | sed -e 's/MemTotal://' -e 's/ kB//'`.to_i / 1024 / 4
        else
            cpus = 2
            mem = 1024
        end

        vb.customize ["modifyvm", :id, "--memory", mem]
        vb.customize ["modifyvm", :id, "--cpus", cpus]
    end

    # Enable provisioning with a shell script
    config.vm.provision "shell", inline: <<-SHELL
        curl -sL https://deb.nodesource.com/setup_4.x | sh

        apt-get install -y g++ git nodejs
        npm install -g npm
        npm cache clean -f

        mkdir /home/vagrant/node_modules
        chmod g+rwx /vagrant
        chown -R vagrant:vagrant /home/vagrant
        cd /vagrant
        rm -rf node_modules
        ln -s /home/vagrant/node_modules /vagrant/node_modules

        npm config set bin-links false

        echo "Australia/Sydney" | tee /etc/timezone
        echo "cd /vagrant" >> /home/vagrant/.bashrc
    SHELL
end
