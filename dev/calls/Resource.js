module.exports = {
  apiVersion: '2015-01-01',
  baseURI: `/resourceGroups/`,

  createResourceGroup:function(resourceGroupName){

    var payload = {
      "location":"Australia East"
    }
    return makeAzureRequest('put', `${this.baseURI}/resourceGroupName`, this.apiVersion, null, payload);}
  }
