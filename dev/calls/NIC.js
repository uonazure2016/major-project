module.exports = {
	apiVersion: '2016-03-30',
	baseURI: `/resourceGroups/${AZURECONFIG.RESOURCE_GROUP}/providers/Microsoft.Network/networkInterfaces`,

	createNIC: function (NICName,publicIP) {
    var base = "/subscriptions/"+AZURECONFIG.SUBSCRIPTION_ID+"/resourceGroups/"+AZURECONFIG.RESOURCE_GROUP+"/providers/Microsoft.Network/";
		var payload =
    {
       "location":"australiaeast",
       "properties":{
          "ipConfigurations":[
             {
                "name":"default",
                "properties":{
                   "subnet":{
                      "id":AZURECONFIG.SUBNET
                   },
                   "privateIPAllocationMethod":"Dynamic",
                   "privateIPAddressVersion":"IPv4",
                   "publicIPAddress":{
                    "id":base +"publicIPAddresses/"+publicIP
                 }
                }
             }
          ],
          "dnsSettings":{
             "internalDnsNameLabel": NICName
          },
          "enableIPForwarding": false
       }
    }

		return makeAzureRequest('put', `${this.baseURI}/${NICName}`, this.apiVersion, null, payload);
	},

	getNICStatus: function (NICName) {
		return makeAzureRequest('get', `${this.baseURI}/${NICName}`, this.apiVersion);
	},

	deleteNIC:function(NICName){
		return makeAzureRequest('del', `${this.baseURI}/${NICName}`, this.apiVersion);
	}

};
