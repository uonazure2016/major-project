module.exports = {
    apiVersion: '2016-03-30',
    baseURI: `/resourceGroups/${AZURECONFIG.RESOURCE_GROUP}/providers/Microsoft.Network/publicIPAddresses`,

    createPublicIPAddress: function(publicIPAddressName) {
        var payload = {
            "location": CONFIG.LOCATION,
            "properties": {
                "publicIPAllocationMethod": "Dynamic",
                "publicIPAddressVersion": "IPv4",
                "idleTimeoutInMinutes": 4
            }
        };

        return makeAzureRequest('put', `${this.baseURI}/${publicIPAddressName}`, this.apiVersion, null, payload);
    },

    getPublicIPStatus: function(publicIPAddressName) {
        return makeAzureRequest('get', `${this.baseURI}/${publicIPAddressName}`, this.apiVersion);
    },

    deletePublicIP: function(publicIPAddressName) {
        return makeAzureRequest('del', `${this.baseURI}/${publicIPAddressName}`, this.apiVersion);
    }
};
