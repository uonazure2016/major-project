module.exports = {
	startStudentVM: function(vmName){
		console.log(vmName);
		Calls.VM.getVMInstanceView(vmName).then(
			Calls.VM.startVM(vmName).catch((error)=>{console.log("Error Starting");})
		).catch((error)=>{
			this.createMachine(vmName,"AdminUsername","Password1234!");

		})
	},
	createMachine: function (name, username, password) {
		var machineModel = {
			name: name,
			ip: '',
			username: username,
			password: password,
			state: 'added'
		};

		// Start this chain of machine creation
		this.createMachineIP(machineModel);
	},

	/*
	 * Step 1: Create an IP for our virtual machine to use
	 */
	createMachineIP: function (machine) {
		Calls.IP.createPublicIPAddress(machine.name)
			.then((res) => {
			  var ensureIPCreation = (res) => {
					if (res.body.properties.provisioningState === 'Succeeded') {
						machine.state = 'ip:created';
						this.createMachineNIC(machine);
					} else {
						machine.state = 'ip:pending';
						global.setTimeout(() => {
							Calls.IP.getPublicIPStatus(machine.name)
								.then(ensureIPCreation)
								.catch((errors) => console.log(errors));
						}, CONFIG.REFRESH_RATE);;
					}
				};

				Calls.IP.getPublicIPStatus(machine.name)
					.then(ensureIPCreation)
					.catch((errors) => console.log(errors));
			})
			.catch((errors) => {
				console.log(errors);
			});
	},

	createMachineNIC: function (machine) {
		Calls.NIC.createNIC(machine.name, machine.name)
		.then((res) => {
			var ensureNICCreation = (res) => {
				if(res.body.properties.provisioningState === "Succeeded"){
					machine.state = 'nic:created';
					this.createMachineVM(machine);
				}else{
					machine.state = 'nic:pending';
					global.setTimeout(() => {
						Calls.NIC.getNICStatus(machine.name)
							.then(ensureNICCreation)
							.catch((errors) => console.log(errors));
					},CONFIG.REFRESH_RATE);
				}
			};
				Calls.NIC.getNICStatus(machine.name)
					.then(ensureNICCreation)
					.catch((errors) => console.log(errors));
		}).catch((errors) => {
			console.log(errors);
		});
	},

	createMachineVM: function (machine) {
		Calls.VM.createVM(machine.name, machine.username,
						  machine.password, machine.name)
		.then((res)=>{
			var ensureVMCreation = (res) =>{
				if(res.body.properties.provisioningState === "Succeeded"){
					Calls.IP.getPublicIPStatus(machine.name)
						.then((res) => {
							machine.ip = res.body.properties.ipAddress;
							machine.state = 'ready';
						})
						.catch((errors) => console.log(errors));
				}else{
					machine.state = 'vm:pending';
					global.setTimeout(() => {
						Calls.VM.getVMStatus(machine.name)
							.then(ensureVMCreation)
							.catch((errors) => console.log(errors));
					},CONFIG.REFRESH_RATE);
				}
			}
			Calls.VM.getVMStatus(machine.name)
				.then(ensureVMCreation)
				.catch((errors) => console.log(errors));
		}).catch((errors) => {
			console.log(errors);
		});
	}
};
