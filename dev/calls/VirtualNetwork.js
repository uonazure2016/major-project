module.exports = {
	apiVersion: '2016-03-30',
	baseURI: `/resourceGroups/${AZURECONFIG.RESOURCE_GROUP}/providers/Microsoft.Compute/virtualMachines`,

	createVirtualNetwork: function (virtualNetworkName,subnetName) {
		var payload = [
       {
          "location":"Australia East",
          "properties":{
             "addressSpace":{
                "addressPrefixes":[
                   "10.1.0.0/16",
                   "10.2.0.0/16"
                ]
             },
             "subnets":[
                {
                   "name": subnetName,
                   "properties":{
                      "provisioningState":"Succeeded",
                      "addressPrefix":"10.1.0.0/24",
                   }
                }
             ]
          }
       }
    ];


		return makeAzureRequest('put', `${this.baseURI}/${virtualNetworkName}`, this.apiVersion, null, payload);
	},

	getPublicIPStatus: function (publicIPAddressName) {
		return makeAzureRequest('get', `${this.baseURI}/${virtualNetworkName}`, this.apiVersion);
	}
};
