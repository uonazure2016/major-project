var azure = require('azure-storage');
var blobService = azure.createBlobService();
//https://github.com/Azure/azure-storage-node
module.exports = {
    apiVersion: '2016-01-01',
    baseURI: `/resourceGroups/${AZURECONFIG.RESOURCE_GROUP}/providers/Microsoft.Storage/`,

    createStorageGroup: function(storageGroupName) {
        var payload = {
            "location": "australiaeast",
            "properties": {
                "accessTier": "Hot"
            },
            "sku": {
                "name": "Standard_LRS"
            },
            "kind": "BlobStorage"
        }
        return makeAzureRequest('put', `${this.baseURI}/storageAccounts/${storageGroupName}`, this.apiVersion, null, payload);

    },
    checkStorageAccountName: function(storageGroupName) {
        var payload = {  
            "name": storageGroupName,
             "type": "Microsoft.Storage/storageAccounts"
        }
        return makeAzureRequest('post', `https://management.azure.com/subscriptions/${AZURECONFIG.SUBSCRIPTION_ID}/providers/Microsoft.Storage/checkNameAvailability`, this.apiVersion, null, payload);
    },

    createContainer: function(containerName) {

        blobService.createContainerIfNotExists(containerName, {
            publicAccessLevel: 'blob'
        }, function(error, result, response) {
            if (!error) {
                return result;
            }
        });
    },
    deleteBlob: function(vmName) {
        blobService.deleteBlob('vhd', vmName + '.vhd', function(error, response) {
            if (!error) {
                // Blob has been deleted
            }
        });
    },
	getStorageAccessKey:function(storageGroupName){
		return makeAzureRequest('post', `${this.baseURI}/storageAccounts/${storageGroupName}/listKeys`, this.apiVersion);

	}

}
