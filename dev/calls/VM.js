module.exports = {
    apiVersion: '2016-03-30',
    baseURI: `/resourceGroups/${AZURECONFIG.RESOURCE_GROUP}/providers/Microsoft.Compute/virtualMachines/`,

    createVM: function(vmName, username, password, NICName) {
		var osLink = AZURECONFIG.WINDOWS_SERVER;
		if(vmName.includes("Desk")){
			osLink = AZURECONFIG.WINDOWS_DESK;
		}else{
			osLink = AZURECONFIG.WINDOWS_SERVER;
		}
        var payload = {
            "id": this.baseURI + vmName,
            "name": vmName,
            "location": "australiaeast",
            "tags": {
                "studentNumber": "thisiswhatiswrong"
            },
            "properties": {
                "hardwareProfile": {
                    "vmSize": "Standard_A1"
                },
                "osProfile": {
                    "computerName": vmName,
                    "adminUsername": "AdminUsername",
                    "adminPassword": "Password1234!"
                },
                "storageProfile": {
                    "osDisk": {
                        "name": "windows-server-2012-r2.vhd",
                        "osType": "Windows",
                        "caching": "ReadWrite",
                        "image": {
                            "uri": osLink
                        },
                        "vhd": {
                            "uri": "https://uonvmstorage.blob.core.windows.net/vhd/" + vmName + ".vhd",
                        },
                        "createOption": "FromImage"
                    },
                },
                "networkProfile": {
                    "networkInterfaces": [{
                        "id": `subscriptions/${AZURECONFIG.SUBSCRIPTION_ID}/resourceGroups/${AZURECONFIG.RESOURCE_GROUP}/providers/Microsoft.Network/networkInterfaces/` + NICName
                    }]
                }
            }
        }

        return makeAzureRequest('put', `${this.baseURI}/${vmName}`, this.apiVersion, null, payload);
    },

    getVMStatus: function(vmName) {
        return makeAzureRequest('get', `${this.baseURI}/${vmName}`, this.apiVersion);
    },

	getAllVms:function(){
		return makeAzureRequest('get',`${this.baseURI}`,this.apiVersion);
	},

    startVM: function(vmName) {
        return makeAzureRequest('post', `${this.baseURI}/${vmName}/start`, this.apiVersion);
    },

    stopVM: function(vmName) {
        return makeAzureRequest('post', `${this.baseURI}/${vmName}/deallocate`, this.apiVersion);
    },

    getVMInstanceView: function(vmName) {
        return makeAzureRequest('get', `${this.baseURI}/${vmName}/InstanceView`, this.apiVersion);
    },

    deleteVM: function(vmName) {
        return makeAzureRequest('del', `${this.baseURI}/${vmName}`, this.apiVersion);
    },

	deleteVmResourses: function(vmName){
		console.log("dvr called");
		this.deleteVM(vmName).then((res)=>
		{
			console.log("waiting for vm deletion");
			global.setTimeout(()=>{
				Calls.NIC.deleteNIC(vmName).then((res)=>{
					console.log("waiting for NIC deletion");
						global.setTimeout(()=>{
							Calls.IP.deletePublicIP(vmName);
							Calls.Storage.deleteBlob(vmName);
						},15000).then(()=>{
							return "sucess";
						}
					)
					}
				).catch((err)=>this.deleteVmResourses(vmName));
			},15000)
		}


	).catch((err)=>this.deleteVmResourses(vmName))
	}
};
