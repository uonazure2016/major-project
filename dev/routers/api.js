var router = Express.Router();
var User = require('../lib/passport/usermodel');

global.Calls = {
    IP: require(CONFIG.SERVER_PATH + '/dev/calls/IP'),
    Local: require(CONFIG.SERVER_PATH + '/dev/calls/Local'),
    NIC: require(CONFIG.SERVER_PATH + '/dev/calls/NIC'),
    VM: require(CONFIG.SERVER_PATH + '/dev/calls/VM'),
    Storage: require(CONFIG.SERVER_PATH + '/dev/calls/Storage'),
    Resource: require(CONFIG.SERVER_PATH + '/dev/calls/Resource')
};

router.use((req, res, next) => {
    console.log('Serving API request...');
    next();
});

router.get('/', (req, res) => {
    res.json({
        message: 'Default API page'
    });
});
router.get('/deallocateall', (req, res) => {
    Calls.VM.getAllVms().then((vmres) => {
		var vmJson = vmres.body.value;
		for(var i in vmJson){
			console.log("Deallocating "+vmJson[i].name);
			Calls.VM.stopVM(vmJson[i].name);
		}
	});
});

router.get('/vm/create', (req, res) => {
    console.log('Called /vm/create...');
    Calls.Local.createMachine(req.query.name, 'AdminUsername', 'Password1234!');
    res.sendFile(path.join(CONFIG.SERVER_PATH + '/www/testing/index.html'));
});

router.post('/vm/delete', (req, res) => {
    var vmName = req.body["vmname"];
    Calls.VM.deleteVmResourses(vmName)
    res.redirect('back');

})

router.post('/vm/start', (req, res) => {
    var vmName = req.body["data"];
    Calls.Local.startStudentVM(vmName);

});

router.post('/vm/stop', (req, res) => {
    var vmName = req.body["data"];
    console.log(vmName);
    Calls.VM.stopVM(vmName);
});

router.post('/vm/list/win', (req, res) => {
    var status;
    var jsonbody = JSON.stringify(req.body);
    var username = req.body["data"];
    var machine = req.body["machine"];
    Calls.VM.getVMInstanceView(username + machine).then(
        (req) => {
            var responseBody = req.body;
            provisional = responseBody.statuses[0]['displayStatus'];
            status = responseBody.statuses[1]['displayStatus'];
            console.log(status);
            res.send("VM Provisional Status:" + provisional + " VM Status:" + status);

        }
    ).catch((errors) => {
        console.log(errors);
        res.send("Status: VM Not Created or Off");
    })
});
router.post('/vm/connect', (req, res) => {
    var ip;
    var vmName = req.body["data"];
    Calls.IP.getPublicIPStatus(vmName).then((req) => {
        var ipAddress = req.body.properties.ipAddress;
        console.log(ipAddress);
    })
});

router.get('/vm/connectpage', (req, res) => {
    res.redirect('/client');
});
/*
router.post('/vm/start', (req, res) => {
    Calls.VM.startVM(req.query.name);
    res.sendFile(path.join(CONFIG.SERVER_PATH + '/www/client/index.html'));
});*/

module.exports = router;
