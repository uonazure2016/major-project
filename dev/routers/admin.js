var azure = require('azure-storage');
var blobSvc = azure.createBlobService();
var containerName = 'vhd';
var crypto = require('crypto');
var fs = require('fs');
var jsonfile = require('jsonfile');
var labinfo = CONFIG.SERVER_PATH + '/www/labinfo.json';
var multiparty = require('multiparty');
var path = require('path');
var progress = require('progress-stream');
var router = Express.Router();
var User = require('../lib/passport/usermodel');
var util = require('util');

router.get('/', isAdmin, function(req, res) {
    res.render('admin/index', {
        user: req.user
    });
})


router.get('/lab', isAdmin, function(req, res) {
    res.render('admin/labmain', {
        user: req.user,
        labinfo: JSON.stringify(labinfo),
    });
})
router.get('/labdelete', isAdmin, function(req, res) {
    labinfo = JSON.parse(fs.readFileSync(CONFIG.SERVER_PATH + '/www/labinfo.json'));
    res.render('admin/deletelab', {
        user: req.user,
        labinfo: JSON.stringify(labinfo),
    });
});


router.get('/labupload', isAdmin, function(req, res) {
    res.render('admin/labupload', {
        user: req.user
    });
});

router.post('/lab/del', function(req, res) {
        labinfo = CONFIG.SERVER_PATH + '/www/labinfo.json';
        var currentLabs = jsonfile.readFileSync(labinfo);
        var labfile = currentLabs["availableLabs"][req.body.labindex].filename;
		//using the async version here because async is better in general
		// and it doesnt really matter when the file gets deleted
        fs.unlink(CONFIG.SERVER_PATH + '/www/labsheet/' + labfile, function(err) {
            if (err) return console.log(err);
            console.log('file deleted successfully');
        });
        currentLabs["availableLabs"].splice(req.body.labindex, 1);
        console.log(currentLabs);
        jsonfile.writeFile(labinfo, currentLabs, function(err) {
            console.error(err)
        });


        res.redirect('/admin/labdelete');
    }

);

router.get('/adduser', isAdmin, function(req, res) {
    res.render('admin/adduser', {
        user: req.user
    });
});

router.post('/adduser', function(req, res) {
    var newuser = new User({
        username: req.body.username,
        password: req.body.username,
        email: req.body.email,
        displayName: req.body.displayName,
		userType:req.body.userType
    });
    User.findOne({
        'username': req.body.username
    }, function(err, user) {
        if (err) { //do nothing
        }
        if (!user) {
            newuser.save(function(err) {
                res.redirect('/admin/addUser');
            });
        }
        if (user) {
            res.send("USER ALREADY EXISTS");
        }
    });


});

router.post('/changepassword', function(req, res) {
    User.findOne({
        'username': req.body.username
    }, function(err, user) {
        if (err) { //do nothing
        }

        if (user) {
            res.send("USER ALREADY EXISTS");
        }
    });
})

router.post('/lab/req', function(req, res) {
    var size = '';
    var fileName = '';

    var form = new multiparty.Form();
    var obj = {
        "labname": "",
        "vmrequired1": "false",
        "vmrequired2": "false",
        "vmrequired3": "false",
        "filename": ""
    };
    form.on('field', function(name, value) {
        obj[name] = value;
        console.log(obj);
    });

    //starts stream
    form.on('part', function(part) {
        if (!part.filename) {
            return;
        }
        var size = part.byteCount;
        var name = part.filename;
    });

    //transfers file from temp location to an actual useful location
    form.on('file', function(name, file) {
        fileName = file.originalFilename;
        var tmp_path = file.path
        var target_path = CONFIG.SERVER_PATH + '/www/labsheet/' + fileName;
        if (fs.existsSync(target_path)) {

            //generates a 20 byte string to change the pdf name too
            // could be better ways of coming up withs names i know
            fileName = crypto.randomBytes(20).toString('hex') + '.pdf'
            target_path = CONFIG.SERVER_PATH + '/www/labsheet/' + fileName;
        }
        //sets up the stream from the temp path to the actual target path .
        //required for cross-device transfers
        var is = fs.createReadStream(tmp_path);
        var os = fs.createWriteStream(target_path);
        is.pipe(os);
        is.on('end', function() {
            fs.unlinkSync(tmp_path);
        });
        console.log('Upload completed!');
        obj["filename"] = fileName;
        var currentLabs = jsonfile.readFileSync(labinfo);
        currentLabs["availableLabs"].push(obj)

        jsonfile.writeFile(labinfo, currentLabs, function(err) {
            console.error(err)
        })
        res.redirect('/lab');
    });

    form.parse(req);
});

router.get('/upload', isAdmin, function(req, res) {
    res.render('admin/uploadvhd', {
        user: req.user
    });
});

router.post('/upload/req', function(req, res) {
    var form = new multiparty.Form();

    form.on('part', function(part) {
        if (!part.filename)
            return;
        var size = part.byteCount;
        var name = part.filename;

        blobSvc.createPageBlobFromStream(containerName, name, part, size, function(err, result, response) {
            if (!err) {
                console.log(response);
            } else {
                console.log(err);
            }
        });
        res.send('File uploaded successfully');
    });
    form.on('progress', function(bytesReceived, bytesExpected) {
        console.log(bytesReceived / bytesExpected * 100);
    });
    form.parse(req);

});

router.get('/vms', isAdmin, function(req, res) {
    var vmList;
    Calls.VM.getAllVms().then((vmResponse) => {
        vmList = vmResponse.body;
        res.render('admin/vmmain', {
            vms: JSON.stringify(vmList),
            user: req.user
        });
    });


});

function isAdmin(req, res, next) {
    //CHECKS in case someone tries to access admin pages without being logged in
    if (req.user === undefined) {
        res.redirect('/');
    }
    if (req.user.userType === 'admin') {
        next();
    } else {
        res.redirect('/');
    }
}

module.exports = router;
