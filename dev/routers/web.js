var router = Express.Router();
var multiparty = require('multiparty');

var async = require('async');
var crypto = require('crypto');
var passport = require('passport');
var util = require('util');
var fs = require('fs');
var labinfolink = CONFIG.SERVER_PATH + '/www/labinfo.json';
var labinfo = CONFIG.SERVER_PATH + '/www/labinfo.json';
var User = require('../lib/passport/usermodel');

var nodemailer = require('nodemailer');
var smtpTransport = require("nodemailer-smtp-transport");

router.use((req, res, next) => {
    console.log('Serving web content...');
    next();
});


router.get('/', function(req, res, next) {
    if (req.user) {
        res.redirect('/main');
    }
    res.render('index', {
        user: req.user,
    });
});


router.get('/login',
    function(req, res) {
        res.render('login', {
            env: process.env
        });
    });

router.post('/login',
    passport.authenticate('local', {
        failureRedirect: '/login'
    }),
    function(req, res) {
        res.redirect('/');
    });

// Perform session logout and redirect to homepage
router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

router.get('/changepassword', loggedIn, function(req, res) {
    res.render('changepassword', {
        user: req.user,
        userType: JSON.stringify(req.user.userType)
    });
});

router.post('/changepassword',loggedIn,function(req,res){
	User.findOne({username:req.user.username},function(err,user){
		if(!user){
			req.flash('error','Error with username');
			return res.redirect('/changepassword');
		}else if(req.body.oldPassword!= user.password){
			req.flash('error','Old password was incorrect');
			return res.redirect('/changepassword');
		}else if(req.body.newPassword!=req.body.confirm){
			req.flash('error','New passwords do not match');
			return res.redirect('/changepassword');
		}else{
			user.password=req.body.newPassword
			user.save();
			req.flash('info','Your password has been changed successfully');
			return res.redirect('/');
		}
	});
});

router.get('/forgetpassword', function(req, res) {
    res.render('resetpassword/forgot');
});

router.post('/forgetpassword', function(req, res, next) {
    async.waterfall([
		//Generates random token string
		function(done){
			crypto.randomBytes(20,function(err,buf){
				var token = buf.toString('hex');
				console.log(token);
				done(err,token)
			})
		},
		function(token,done){
			User.findOne({email:req.body.email},function(err,user){
				if(!user){
					req.flash('error','No Account with that email address exists');
					return res.redirect('/forgetpassword');
				}
				user.resetPasswordToken = token;
				//Set token expiry to 15 minutes after this request
				user.resetPasswordExpires=Date.now()+900000;
				user.save(function(err){
					done(err,token,user)
				});
			})

		},
		function(token,user,done){
			var transport = nodemailer.createTransport(smtpTransport(
				{
					service:'gmail',
					auth:{
						user:'azureprojectpwordreset@gmail.com',
						pass:'turkeysub'
					}
				}
			));

			var mailOptions = {
				to:user.email,
				from:AZURECONFIG.RESET_EMAIL_ADDRRESS,
				subject: 'A request for a new password has been made for your account',
				text: 'Hi '+user.displayName+'.\n\n A request has been made to create a new password'+
				'for your account. If you have made this request please click the following link to'+
				'complete this process \n'+
				'http://'+req.headers.host+'/reset/'+token +'\n\n'+
				'Please ignore this if you did not make this request'
			};
			transport.sendMail(mailOptions,function(err){
				req.flash('info','An email has been sent to '+user.email);
				done(err,'done');
			})
		}

    ], function(err) {
        if (err) return next(err);
        res.redirect('/forgetpassword');
    });
});

router.get('/reset/:token', function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgetpassword');
    }
    res.render('resetpassword/reset', {
      user: req.user
    });
  });
});

router.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }
		if(req.body.password==req.body.confirm){
			user.password = req.body.password;
	        user.resetPasswordToken = undefined;
	        user.resetPasswordExpires = undefined;
		}else{
			req.flash('error','Passwords did not match');
			return res.redirect('back');
		}



        user.save(function(err) {
          done(err,user);
        });
      });
    },
    function(user, done) {
		var transport = nodemailer.createTransport(smtpTransport(
			{
				service:'gmail',
				auth:{
					user:'azureprojectpwordreset@gmail.com',
					pass:'turkeysub'
				}
			}
		));
      var mailOptions = {
        to: user.email,
        from: AZURECONFIG.RESET_EMAIL_ADDRRESS,
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      transport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });
});

router.get('/main', loggedIn, function(req, res) {
    res.render('main', {
        user: req.user,
        labinfo: labinfo,
        userType: JSON.stringify(req.user.userType)
    });
});

router.get('/lab', loggedIn, function(req, res) {
    fs.readFile(labinfolink, 'utf8', function(err, contents) {
        console.log('after calling readFile');
        templabinfo = JSON.parse(contents);
        res.render('lab', {
            user: req.user,
            labinfo: JSON.stringify(templabinfo),
            userType: JSON.stringify(req.user.userType)
        });
    })


});


router.get('/labpage', loggedIn, function(req, res) {
    //  console.log(req);
    res.render('labview', {
        user: JSON.stringify(req.user.username),
        pdf: "Lab5.pdf",
        link: '/labsheet/',
        userType: JSON.stringify(req.user.userType)
    })

});

router.post('/rdpClient', loggedIn, function(req, res) {
    var vmName = req.body.vmName;
	var username="AdminUsername";
	var password="Password1234!"
	if(vmName.includes("Desk")){
		username = "Admin";
		password = "password";
	}
    Calls.IP.getPublicIPStatus(vmName).then((req) => {
        var ipAddress = req.body.properties.ipAddress;
        res.render('client', {
            user: req.user,
            ip: JSON.stringify(ipAddress),
			username:JSON.stringify(username),
			password:JSON.stringify(password)

        })

    })


});

router.post('/labpage', loggedIn, function(req, res) {
    console.log(req.body);
    labinfo = JSON.parse(fs.readFileSync(CONFIG.SERVER_PATH + '/www/labinfo.json'));
    res.render('labview', {
        userId: req.user,
		userType: JSON.stringify(req.user.userType),
        user: JSON.stringify(req.user.username),
        pdf: JSON.stringify(req.body.filename),
        link: '/labsheet/',

    })
});

function loggedIn(req, res, next) {
    if (req.user) {
        next();
    } else {
        res.redirect('/');
    }
}

module.exports = router;
