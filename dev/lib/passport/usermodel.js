var mongoose = require('mongoose');

module.exports = mongoose.model(
	'User',{
		username:String,
		password:String,
		email:String,
		displayName:String,
		resetPasswordToken:String,
		resetPasswordExpires:Date,
		userType:String,
		vmDesk1Status:String,
		vmDesk2Status:String,
		vmServStatus:String
	});
